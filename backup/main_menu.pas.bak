unit main_menu;
{---------------------------------------------------------}
{|                                                       |}
{|                                                       |}
{|                    Ho Lap College                     |}
{|          ICT SBA Part D : Software Developement       |}
{|            A Cashless Point-of-Sales System           |}
{|                 Name: Yeung Kai Chun                  |}
{|                   Class: F5D / F6D                    |}
{|                                                       |}
{|                                                       |}
{---------------------------------------------------------}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, Grids, Menus, ValEdit, Spin, Windows;

type

  { TPOS }
  customers_base = record
    id:string[8];
    class_name,class_no:string[2];
    customer_name:string[20];
    gender,is_staff:char;
    balance:real;
    end;
  items_base = record
    code:string[4];
    name:String[25];
    origin:string[26];
    import_price,selling_price:real;
    quantity, beginningpointer, endpointer:integer;
    purchase:boolean;
    end;
  TPOS = class(TForm)
    add_cart: TButton;
    use_discount_btn: TCheckBox;
    Checkout_btn: TButton;
    Cart_Access: TButton;
    Grand_Total_Des: TLabel;
    Select_Description: TLabel;
    select_quantity: TLabel;
    Price_Show: TLabel;
    Logout_btn: TButton;
    cancel_highlight: TButton;
    acc_input: TEdit;
    Name_Welcome: TLabel;
    Rice_Group: TCheckGroup;
    Sandwich_Group: TCheckGroup;
    Drink_Group: TCheckGroup;
    SandF_Group: TCheckGroup;
    select_highlight: TLabel;
    POS_Tell: TLabel;
    Page_Selection: TPageControl;
    Drink: TRadioButton;
    SandF: TRadioButton;
    Rice: TRadioButton;
    Sandwich: TRadioButton;
    School_Name: TLabel;
    Main_Menu_Des: TTabSheet;
    Shape1: TShape;
    acc_input_des: TStaticText;
    Quantity_Helper: TSpinEdit;
    Shape2: TShape;
    Stock: TTabSheet;
    info_table: TStringGrid;
    Purchase: TTabSheet;
    Latest_Selection: TStringGrid;
    procedure acc_inputChange(Sender: TObject);
    procedure add_cartClick(Sender: TObject);
    procedure cancel_highlightClick(Sender: TObject);
    procedure Cart_AccessClick(Sender: TObject);
    procedure Checkout_btnClick(Sender: TObject);
    procedure DrinkClick(Sender: TObject);
    procedure Drink_GroupItemClick(Sender: TObject; Index: integer);
    procedure FormActivate(Sender: TObject);
    procedure Logout_btnClick(Sender: TObject);
    procedure PurchaseHide(Sender: TObject);
    procedure PurchaseShow(Sender: TObject);
    procedure Quantity_HelperChange(Sender: TObject);
    procedure RiceClick(Sender: TObject);
    procedure Rice_GroupItemClick(Sender: TObject; Index: integer);
    procedure SandFClick(Sender: TObject);
    procedure SandF_GroupItemClick(Sender: TObject; Index: integer);
    procedure SandwichClick(Sender: TObject);
    procedure Sandwich_GroupItemClick(Sender: TObject; Index: integer);
    procedure StockShow(Sender: TObject);
    procedure use_discount_btnChange(Sender: TObject);
  private

  public
     dirc_locate : string;
     Current_User_Info : customers_base;
     is_checkingout : boolean;
     db_items : array[0..500] of items_base;
     db_customer : array[0..500] of customers_base;
     no_of_items : integer; //number of items in the database
     no_of_customers : integer; //number of students in the database
  end;

var
  POS: TPOS;
implementation
uses Cart_Display,Manually_Select_Directory;
type
  pointers_of_each_type = record
    beginningpointer, endpointer : integer;
  end;

var
  no_of_rice,no_of_sandf,no_of_sandwich,no_of_drink : integer;
  drink_pointer, sandf_pointer, rice_pointer, sandwich_pointer : pointers_of_each_type;
  //Containing the index of items pointer to classify starting/ending pointer of every items' type
{$R *.lfm}

{ TPOS }

function deletingspace(tempstr:string):string;
{A function that remove the blank space at the end of tempstr}
var
  pointers: integer;
  difference : boolean = false;
begin
    pointers := length(tempstr); //Pointing at the end of tempstr
      while (not difference) and (pointers >= 1) do begin
        if tempstr[pointers] <> ' ' then begin
          difference := true;
          tempstr := copy(tempstr,1,pointers);
        end
        else
          dec(pointers);
      end;
      deletingspace := tempstr;
  end;

function find_begin(findletter:String):integer;
//function that find the beginning of a specific items type
var
  found : boolean = false;
  r_value : integer = 0;
begin
  while not found do begin
    if findletter = pos.db_items[r_value].code[1] then //When found matched first letter
       found := true
    else
       inc(r_value);
  end;
  find_begin := r_value; //Returning functions' required data
end;

function find_end(findletter:String; beginpointer:integer):integer;
//function that find the end of a specific items type
var
  found : boolean = false;
  r_value : integer;
begin
  r_value := beginpointer;
  while not found do begin
    if findletter <> pos.db_items[r_value].code[1] then //When detected a change in first letter
       found := true
    else
       inc(r_value);
  end;
  find_end := r_value; //Returning functions' required data
end;

procedure displaydefault;
var
  infile : text;
  i : integer = 0;
  j : integer;
begin
  if POS.dirc_locate = '' then  //Test if files are correctly targeted
    if (not fileexists('J:\SBA\2020\DB\items.txt')) then begin
       ManualDirectory.Show;
    end
    else
       POS.dirc_locate := 'J:\SBA\2020\DB\';
    system.assign(infile, POS.dirc_locate + '\items.txt');
    system.reset(infile);
    while not eof(infile) do begin
      with pos.db_items[i] do
           system.read(infile,code,name,origin,import_price,selling_price,quantity);
      system.readln(infile);
      inc(i);
    end;
    system.close(infile);  //Closing file to prevent memeory loss
    pos.no_of_items := i;
    for i := 0 to pos.no_of_items do
        pos.db_items[i].name := deletingspace(pos.db_items[i].name);
    pos.info_table.rowcount := pos.no_of_items + 1;
    for i := 1 to 4 do begin
      for j := 1 to pos.no_of_items do begin
        case i of  //Write data into cells according to the value i
          1 : pos.info_table.Cells[i,j] := pos.db_items[j - 1].code;
          2 : pos.info_table.Cells[i,j] := pos.db_items[j - 1].name;
          3 : pos.info_table.Cells[i,j] := '$'+ floattostr(pos.db_items[j - 1].selling_price);
          4 : pos.info_table.Cells[i,j] := inttostr(pos.db_items[j - 1].quantity);
        end; //End of case
      end;
    end; //End of cells value placement
end;

procedure TPOS.StockShow(Sender: TObject);//Read data from DB and display on different cells
begin
   displaydefault; //When form is shown, read DB and deplay all info
end;

procedure assigning_cells(beginningpointer,endpointer:integer);
//A procedure that scale a table and set the cells' values in the table
var
  j,k:integer;
begin
  pos.info_table.rowcount := endpointer - beginningpointer + 1;
  for j := 1 to 4 do begin
         for k := 1 to (endpointer - beginningpointer) do begin
             case j of  //Write data into cells according to the value i
                  1 : pos.info_table.Cells[j,k] := pos.db_items[beginningpointer + k - 1].code;
                  2 : pos.info_table.Cells[j,k] := pos.db_items[beginningpointer + k - 1].name;
                  3 : pos.info_table.Cells[j,k] := '$'+ floattostr(pos.db_items[beginningpointer + k - 1].selling_price);
                  4 : pos.info_table.Cells[j,k] := inttostr(pos.db_items[beginningpointer + k - 1].quantity);
             end; //End of case
         end;
     end;
end;

procedure TPOS.RiceClick(Sender: TObject);
begin
   assigning_cells(no_of_drink + no_of_sandf + 1,no_of_drink + no_of_sandf + no_of_rice);
end;

procedure TPOS.SandFClick(Sender: TObject);
begin
   assigning_cells(no_of_drink + 1 ,no_of_drink + no_of_sandf);
end;

procedure TPOS.SandwichClick(Sender: TObject);
begin
   assigning_cells(no_of_drink + no_of_sandf + no_of_rice + 1,no_of_items);
end;

procedure TPOS.DrinkClick(Sender: TObject);
begin
   assigning_cells(0, no_of_drink);
end;

procedure TPOS.FormActivate(Sender: TObject);
var
  beginningpointer, endpointer : integer;
begin
  Page_Selection.ActivePage := Stock;
  {Because reading customers' information has dependency on activiating items avaliable
  first as shwon in procedure TPOS.PurchaseShow, therefore stock must be opened first
  to aviod lacking of data}
  displaydefault;  //Re-execute this procedure to read in data
   beginningpointer := find_begin('R');
   endpointer := find_end('R',beginningpointer);
   no_of_rice := endpointer - beginningpointer;
   rice_pointer.beginningpointer := beginningpointer;
   rice_pointer.endpointer := endpointer;
   beginningpointer := find_begin('F');
   endpointer := find_end('F',beginningpointer);
   no_of_sandf := endpointer - beginningpointer;
   sandf_pointer.beginningpointer := beginningpointer;
   sandf_pointer.endpointer := endpointer;
   beginningpointer := find_begin('S');
   endpointer := find_end('S',beginningpointer);
   no_of_sandwich := endpointer - beginningpointer;
   sandwich_pointer.beginningpointer := beginningpointer;
   sandwich_pointer.endpointer := endpointer;
   beginningpointer := find_begin('D');
   endpointer := find_end('D',beginningpointer);
   no_of_drink := endpointer - beginningpointer;
   drink_pointer.beginningpointer := beginningpointer;
   drink_pointer.endpointer := endpointer;
   Page_Selection.ActivePage := Main_Menu_Des; //Showing Main Menu default
   no_of_items := 0;
   no_of_customers := 0; //initialize these two variables
end;

procedure TPOS.cancel_highlightClick(Sender: TObject);
begin
   SandF.checked := false;
   Rice.checked := false;
   Sandwich.checked := false;
   Drink.checked := false;  //Unchecking all button activated
   displaydefault; //reseting view to default
end;

procedure TPOS.Cart_AccessClick(Sender: TObject);
begin
  CartForm.Showmodal;
  CartForm.Height := 756;
  CartForm.Width := 526; //Rescale the form so that checkout button does not show
  is_checkingout := false;
  cartform.confirm_pay.Caption := '';     //Hide all checkout buttons
  cartform.confirm_true.Visible := false;
  cartform.confirm_false.Visible := false;
end;

procedure TPOS.Checkout_btnClick(Sender: TObject);
begin
  is_checkingout := true;
  with CartForm do begin
   Height := 840; //Give little extra space for further confirmation
   Showmodal;
   confirm_pay.Caption := 'Confirm Paying ' + Total_Price.Caption + '?';
   confirm_true.Visible := true;  //Show all checkout buttons
   confirm_false.Visible := true;
  end;
end;

procedure clearpurchasecache; //Procedure that clear all temp. login status
var
  i : integer;
begin
  with pos do begin
    Drink_Group.Enabled := false;
    SandF_Group.Enabled := false;
    Rice_Group.Enabled := false;
    Sandwich_Group.Enabled := false;
    Logout_btn.Visible := false;
    acc_input.text := '';
    Name_Welcome.Caption := '';
    price_show.Caption := '$0.0';
    for i := 0 to no_of_items do
        db_items[i].purchase := false;
    Quantity_Helper.Value := 1;
    Quantity_Helper.ReadOnly := true;
    Latest_Selection.Clean([gzNormal, gzFixedRows]); //Clean all cells except header
    CartForm.Main_List.Clean([gzNormal, gzFixedRows]); //Clean all cells except header
    CartForm.Main_List.RowCount := 1;
    CartForm.Total_Price.Caption := '$0.0';
    Price_Show.Caption := '$0.0';
    use_discount_btn.Enabled := false; //Reset discount button
    use_discount_btn.Checked := false;
    Cartform.confirm_pay.Caption := '';
    Cartform.confirm_true.Visible := false;
    Cartform.confirm_false.Visible := false;
    Cartform.Height := 756;
    ZeroMemory(@current_user_info,SizeOf(current_user_info)); //Clear all current user info
  end;
end;

procedure init_and_clear_purchase_page;
{Clear all checkbox upon exit to prevent duplicated items}
{Also clear some error text upon initial creation of purchase page}
begin
  with pos do begin
   Drink_Group.items.Clear;
   SandF_Group.items.Clear;
   Rice_Group.items.Clear;
   Sandwich_Group.items.Clear;
   Quantity_Helper.Value := 1;
   Quantity_Helper.ReadOnly := true;
  end;
end;

procedure TPOS.PurchaseShow(Sender: TObject); //Displaying avaliable db_items
var
  i: integer;
  DB : text;
begin
  if POS.dirc_locate = '' then  //Test if files are correctly targeted
    if (not fileexists('J:\SBA\2020\DB\items.txt')) then
       ManualDirectory.Show
    else
       POS.dirc_locate := 'J:\SBA\2020\DB\';
  displaydefault;
  init_and_clear_purchase_page;
  //^^^ clear some error text upon initial creation of purchase page
  with pos do begin
    for i := 0 to no_of_items do
        case db_items[i].code[1] of
             'D': begin
             Drink_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then //Disable to check if none quantity reamaining
               Drink_Group.CheckEnabled[i] := false;
             end;
             'F': begin
             sandf_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               sandf_Group.CheckEnabled[Drink_Group.Columns + i] := false;
             end;
             'R': begin
             Rice_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               Rice_Group.CheckEnabled[Drink_Group.Columns + sandf_Group.Columns + i] := false;
             end;
             'S': begin
             Sandwich_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               Sandwich_Group.CheckEnabled[Drink_Group.Columns + sandf_Group.Columns + Rice_Group.Columns + i] := false;
             end;
        end;
    Drink_Group.Enabled := false;
    SandF_Group.Enabled := false;
    Rice_Group.Enabled := false;
    Sandwich_Group.Enabled := false;
  end;
  //Reading all customers' infomation
  system.assign(DB, POS.dirc_locate + '\customer.txt');
  system.reset(DB);
  while not eof(DB) do begin
    with db_Customer[no_of_customers] do
        system.read(DB,id,class_name,class_no,customer_name,gender,is_staff,balance);
    system.readln(DB);
    inc(no_of_customers);
    end;
  system.close(DB);
  for i := 0 to no_of_items do
        db_items[i].purchase := false;
  for i := 0 to no_of_customers do
        db_Customer[i].customer_name := deletingspace(db_Customer[i].customer_name);
end;

procedure TPOS.Quantity_HelperChange(Sender: TObject);
{If user requested an unreasonable quantity of a specific items, i.e. choose below 1
 or choose higher than the avaliable quantity, reset quantity requested}
begin
  if Quantity_Helper.Value <= 0 then begin
    Quantity_Helper.Value := 1;
    showmessage('Requested quantity CANNOT be lower then 1! ');
  end
  else if Quantity_Helper.Value > strtoint(Latest_Selection.Cells[3,1]) then begin
    Quantity_Helper.Value := 1;
    showmessage('Requested quantity exceed avaliable quantity! ');
  end;
end;

procedure recalculate_grand_price;
var
  i : integer;
 temp_price : real = 0;
begin
  for i := 1 to CartForm.Main_List.RowCount - 1 do
     temp_price := temp_price + strtofloat(CartForm.Main_List.Cells[4,i]);
  if pos.use_discount_btn.Checked then
    temp_price := temp_price * 0.9; //Give 10% off discount
  Cartform.Total_Price.Caption := '$' + floattostr(temp_price);
  pos.Price_Show.Caption := '$' + floattostrf(temp_price,ffFixed,1,1); //Only show 1 d.p.
end;

procedure changecart(items_pointer:integer);
var
 i : integer = 1;
 found : boolean = false;
begin
  with CartForm.Main_List do begin
   if pos.Latest_Selection.Cells[1,1] <> '' then begin
     while (not found) and (i <= RowCount  - 1) and (RowCount >= 2) do begin //Find if the item awaiting to be added is dupilcated in Cart
       if pos.Latest_Selection.Cells[2,1] = Cells[2,i] then
          found := true
       else
          inc(i);
     end;
   if found then begin//Add requsted quantity to the current quantity, then display it
       Cells[3,i] := inttostr(strtoint(Cells[3,i]) + pos.Quantity_Helper.Value);
       if strtoint(Cells[3,i]) > pos.db_items[items_pointer].quantity then begin
          Cells[3,i] := inttostr(pos.db_items[items_pointer].quantity);
          showmessage('Requested quantity exceed avaliable quantity! Resetting requested quantity to maximum avalible quantity');
       end;
       Cells[4,i] := floattostr(pos.db_items[items_pointer].selling_price * strtoint(Cells[3,i]));
       //Recalculate SubTotal
       recalculate_grand_price;
   end
   else begin
       RowCount := RowCount + 1;
       Cells[1,RowCount - 1] := pos.Latest_Selection.Cells[1,1];
       Cells[2,RowCount - 1] := pos.Latest_Selection.Cells[2,1];
       Cells[3,RowCount - 1] := inttostr(pos.Quantity_Helper.Value);
       Cells[4,RowCount - 1] := floattostr((pos.db_items[items_pointer].selling_price) *
                             pos.Quantity_Helper.Value);
       recalculate_grand_price;
       pos.Latest_Selection.Cells[4,1] := 'In Cart';
       pos.Latest_Selection.Columns.Items[3].Font.Color:= clLime;
       pos.db_items[items_pointer].purchase := true;
     end;
   end
   else
     showmessage('No Items were selected.');
  end;
end;

function Capitalize_first_letter_of_id:string;
var
  tempchar:char;
begin
  tempchar := pos.acc_input.text[1];
  if tempchar = 's' then
     tempchar := chr(ord(tempchar) - 32);
  Capitalize_first_letter_of_id := tempchar + copy(pos.acc_input.text,2,8);
end;

procedure TPOS.use_discount_btnChange(Sender: TObject);
begin
  if use_discount_btn.Checked then //Notifying that the user is currently using staff discount
    cart_display.CartForm.use_discount_notice.Caption := 'Using staff discount, 10% off'
  else
    cart_display.CartForm.use_discount_notice.Caption := '';
  recalculate_grand_price; //Showing price after discount / cancelling discount
  is_checkingout := true;
end;

procedure TPOS.PurchaseHide(Sender: TObject);
begin
  init_and_clear_purchase_page; //Clear pages upon changing of tabs
  clearpurchasecache; //Disabling all buttons and wait for login
end;

procedure TPOS.acc_inputChange(Sender: TObject);
var
  found : boolean = false;
  i : integer = 0;
  temp : string[8]; //To store a temperaily ID with the first S being capitalized
begin
  if length(acc_input.text) = 8 then begin
     temp := Capitalize_first_letter_of_id; //Capitalize the 's' in the first letter of customer.id
     while (not found) and (i <= no_of_customers) do begin
       if db_customer[i].id = temp then begin
         found := true;
         Current_User_Info := db_customer[i]; //Passing the customer's info to checkout form
         Name_Welcome.Caption := 'Welcome, ' + db_customer[i].customer_name + '!';
         Drink_Group.Enabled := true;
         SandF_Group.Enabled := true;
         Rice_Group.Enabled := true;
         Sandwich_Group.Enabled := true;
         Logout_btn.Visible := true;
         if db_customer[i].is_staff = 'Y' then //Allow user to use discount if they are staffs
           use_discount_btn.Enabled := true;
       end
       else
         inc(i);
     end;
  end;
  if (length(acc_input.text) = 8) and (not found) then
     {If the user has enter an 8-digit ID but does not match with the database,
     return this sentence}
     Name_Welcome.Caption := 'ID not found. Please confirm your id!';
end;

procedure TPOS.add_cartClick(Sender: TObject);
var
  i : integer = 0;
  found : boolean = false;
begin
  while (not found) and (I <= no_of_items) and (Latest_Selection.Cells[4,1] <> 'Removed') do
      {If the user de-selected an item and try to add to cart again, prevent it
      from adding it into the cart as checkbox will be messed up}
      if db_items[i].code = Latest_Selection.Cells[1,1] then begin
        changecart(i);
        found := true;
      end
      else
      inc(i);
end;

procedure TPOS.Logout_btnClick(Sender: TObject);
begin
  clearpurchasecache; //Clear all info when the user click logout
end;

procedure SelectedItemsChange(increaseprice:boolean;i:integer);
var
  j : integer = 1;
  k : integer = 0;
  found : boolean = false;
begin
  with pos do begin
    Quantity_Helper.Value := 1;
    Quantity_Helper.ReadOnly := false;
    Quantity_Helper.MaxValue := db_items[i].quantity;
    if Latest_Selection.Cells[4,1] = 'Pending' then begin //If user didn't put the previous item into cart, uncheck that items
       while (not found) and (k <= Drink_Group.Items.Count - 1) do  //Search in Drink group
           if Latest_Selection.Cells[2,1] = Drink_Group.Items[k] then begin
             found := true;
             Drink_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= SandF_Group.Items.Count - 1) do //Search in SandF Group, skip if found in Drink group
           if Latest_Selection.Cells[2,1] = SandF_Group.Items[k] then begin
             found := true;
             SandF_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= Rice_Group.Items.Count - 1) do  //Search in Rice Group, skip if found in Drink or SandF group
           if Latest_Selection.Cells[2,1] = Rice_Group.Items[k] then begin
             found := true;
             Rice_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= Sandwich_Group.Items.Count - 1) do //Search in Sandwich Group, skip if found in Drink, SandF or Rice group
           if Latest_Selection.Cells[2,1] = Sandwich_Group.Items[k] then begin
             found := true;
             Sandwich_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
    end;
    found := false; //Reset found to False to prevent error if removing a cart items
    if increaseprice then begin //If the users selected another items
       Latest_Selection.Cells[1,1] := db_items[i].code;  //Adding information to latest selection
       Latest_Selection.Cells[2,1] := db_items[i].name;
       Latest_Selection.Cells[3,1] := inttostr(db_items[i].quantity);
       Latest_Selection.Columns.Items[3].Font.Color:= $0011A2DF;
       Latest_Selection.Cells[4,1] := 'Pending';
    end
    else begin //If the users de-selected an items
       Quantity_Helper.Value := 1;
       Latest_Selection.Columns.Items[3].Font.Color:= clRed;
       Latest_Selection.Cells[4,1] := 'Removed';
       if CartForm.Main_List.RowCount <> 2 then begin //Only execute these code if there's more than 1 item in the cart
         while (not found) and (j <= CartForm.Main_List.RowCount - 1) do begin
             //Removing items from cart and shifting others items blow to the top
           if CartForm.Main_List.Cells[2,j] = db_items[i].name then begin
             db_items[i].purchase := false;
             if j = CartForm.Main_List.RowCount - 1 then //If the items that need to be removed is on the last row
               CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1
             else begin
              for k := j to CartForm.Main_List.RowCount - 1 do begin
                  with CartForm.Main_List do begin
                     Cells[1,k-1] := Cells[1,k]; //shifting the next cells' value to the current cells
                     Cells[2,k-1] := Cells[2,k];
                     Cells[3,k-1] := Cells[3,k];
                     Cells[4,k-1] := Cells[4,k];
                  end;
              end;
              found := true;
              CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1; //Delete Last Row
             end;
           end
           else
               inc(j);
         end;
      end
       else if CartForm.Main_List.RowCount = 2 then begin
         CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1; //If there's only 1 item in the cart, just remove a row
         Cartform.Total_Price.Caption := '$0.0'; //Reseting Grand Total to $0.0
       end;
      recalculate_grand_price;
    end;
  end;
end;

procedure TPOS.Drink_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Drink_Group.Items.Count - 1 do begin
     if (Drink_Group.Checked[i]) and (not db_items[i + drink_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i)
    else if (not Drink_Group.Checked[i]) and (db_items[i + drink_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i);
  end;
end;
procedure TPOS.SandF_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to SandF_Group.Items.Count - 1 do begin
     if (SandF_Group.Checked[i]) and (not db_items[i + SandF_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + SandF_pointer.beginningpointer)
    else if (not SandF_Group.Checked[i]) and (db_items[i + SandF_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + SandF_pointer.beginningpointer);
  end;
end;

procedure TPOS.Rice_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Rice_Group.Items.Count - 1 do begin
     if (Rice_Group.Checked[i]) and (not db_items[i + Rice_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + Rice_pointer.beginningpointer)
    else if (not Rice_Group.Checked[i]) and (db_items[i + Rice_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + Rice_pointer.beginningpointer);
  end;
end;
procedure TPOS.Sandwich_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Sandwich_Group.Items.Count - 1 do begin
     if (Sandwich_Group.Checked[i]) and (not db_items[i + Sandwich_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + Sandwich_pointer.beginningpointer)
    else if (not Sandwich_Group.Checked[i]) and (db_items[i + Sandwich_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + Sandwich_pointer.beginningpointer);
  end;
end;
end.
