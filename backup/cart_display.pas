unit Cart_Display;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Grids, StdCtrls;

type

  { TCartForm }

  TCartForm = class(TForm)
    confirm_false: TButton;
    confirm_true: TButton;
    Cart: TLabel;
    Grand_Total_Des: TLabel;
    confirm_pay: TLabel;
    use_discount_notice: TLabel;
    Total_Price: TLabel;
    Main_List: TStringGrid;
    procedure confirm_falseClick(Sender: TObject);
    procedure confirm_trueClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  CartForm: TCartForm;

implementation
uses main_menu,dos;
{$R *.lfm}

{ TCartForm }
function addspacebacktoname(temps:string):string;
begin
  while length(temps) < 25 do
    temps := temps + ' ';
  addspacebacktoname := temps;
end;
function add_zero_behind_transnum(converting:string):string; //Converting transact reference number to 4 digit
  //Pointer that get the latest transact number and neglect 0 in front of the transact number
begin
  while length(converting) < 4 do
    converting := '0' + converting;
  add_zero_behind_transnum := converting;
end;
function add_zero_to_date(tempword:string):string;
//Adding zero in front of month or day if there's insufficient number of digits
begin
	if length(tempword) < 2 then //if only one digit, add zero in front of the tempstr
    add_zero_to_date := '0' + tempword;
end;

procedure TCartForm.confirm_trueClick(Sender: TObject);
var
  i : integer = 0;
  found : boolean = false;
  j : integer = 0;
  outfile : text;
  currenttransnumber : string;
  {transactdetail : array[0..9999] of record
     transnumber : string;
     date , ssdno : string;
     transact_money : real;
     boughtitems : array[0..100] of string;
     boughtquantity : array[0..100] of integer;
  end;}
  tempstr,transnumstr : string;
  yy,mm,md,wd : word;
begin
  if (strtofloat(copy(pos.Price_Show.Caption,2,length(pos.Price_Show.Caption))) > pos.current_user_info.balance) then
    //If the user does not have enough balance in their account
    showmessage('Insuffient balance in your account! You only have $'+
                floattostr(pos.current_user_info.balance) +'. Please consider discarding few items!' )
  else if strtofloat(copy(pos.Price_Show.Caption,2,length(pos.Price_Show.Caption))) = 0 then //If no items in cart
    showmessage('No items were selected')
  else begin //Begin the transaction process if enough outstanding balance is in their account
    pos.current_user_info.balance := pos.current_user_info.balance
    - (strtofloat(copy(Total_Price.Caption,2,length(Total_Price.Caption))));
    showmessage('Transaction complete! You now have $' + floattostr(pos.current_user_info.balance) + ' left in your balance');
    while (not found) and (i <= pos.no_of_customers) do begin //Find the user pointer by comparing ID
      if pos.db_customer[i].id = pos.current_user_info.id then begin
        found := true;
        pos.db_customer[i].balance := pos.db_customer[i].balance - strtofloat(copy(Total_Price.Caption,2,length(Total_Price.Caption))); //Deduct balance from the user account
      end
      else
        inc(i);
    end;
    system.assign(outfile,pos.dirc_locate + '\transact.txt'); //Adding the purchasing record to the database
    system.reset(outfile);
    while not eof(outfile) do begin
      inc(j);
      readln(outfile,tempstr);
      transnumstr := copy(tempstr,1,4); //Keep replacing this variable until the last transaction record
    end;
    currenttransnumber := inttostr((strtoint(transnumstr) + 1));
    system.close(outfile);
    system.Append(outfile);
    getdate(yy,mm,md,wd);
    system.append(outfile);
    system.write(outfile,add_zero_behind_transnum(currenttransnumber),' ',inttostr(yy)+
    add_zero_to_date(inttostr(mm))+add_zero_to_date(inttostr(md)),' ',pos.db_customer[i].id
    ,copy(Total_Price.Caption,2,length(Total_Price.Caption)):7,' ');
    for i := 1 to Cartform.Main_List.RowCount - 1 do
      with Cartform.Main_List do
      	system.write(outfile,Cells[1,i],' ',Cells[3,i],' ');
    system.writeln(outfile);
    system.close(outfile);
    j := 0;
    for i := 1 to (Main_List.RowCount - 1) do begin //Remove quantity in the database
        found := false;
       while (not found) and (j <= pos.no_of_items - 1) do begin
         if Main_List.Cells[1,i] = pos.db_items[j].code then begin
            pos.db_items[j].quantity := pos.db_items[j].quantity - strtoint(Main_list.Cells[3,i]);
            found := true;
         end
         else
             inc(j);
         end;
         j := 0; //reset j pointer for next items searching
    end;
    system.assign(outfile,pos.dirc_locate + '\customer.txt'); //Writing back new customer data to database
    system.rewrite(outfile);
    for i := 0 to pos.no_of_customers - 1 do begin
      with pos.db_customer[i] do
        system.write(outfile,id,class_name,class_no,customer_name,'':(20-length(customer_name)),gender,is_staff,' ',balance:0:1);
      system.writeln(outfile); //Move pointer to next line
    end;
    system.close(outfile);
    system.assign(outfile,pos.dirc_locate + '\items.txt');
    system.rewrite(outfile);
    for i := 0 to pos.no_of_items - 1 do begin
      with pos.db_items[i] do begin
        system.write(outfile,code,addspacebacktoname(name),addspacebacktoname(origin)
        ,import_price:5:1,selling_price:5:1,quantity:3);
      end;
      system.writeln(outfile); //Move pointer to next line
    end;
    system.close(outfile);
  end; //end of overwriting of files
  pos.clearpurchasecache; //Logout from account after purchase
  pos.init_and_clear_purchase_page;
  pos.read_in_necessary_files;
  Cartform.Close;
end;

procedure TCartForm.confirm_falseClick(Sender: TObject);
begin
  Cartform.Close;
end;

procedure TCartForm.FormResize(Sender: TObject);
begin
  if pos.is_checkingout then //Force to rescale the form if the user try to expand or shrink it
    CartForm.Height := 840
  else
    CartForm.Height := 759;
end;

procedure TCartForm.FormShow(Sender: TObject);
begin
  if pos.is_checkingout then begin
   Height := 840; //Give little extra space for further confirmation
   confirm_pay.Caption := 'Confirm Paying ' + pos.Price_Show.Caption + '?';
   confirm_true.Visible := true;  //Show all checkout buttons
   confirm_false.Visible := true
  end
  else begin
   Height := 756; //Give little extra space for further confirmation
   confirm_pay.Caption := '';
   confirm_true.Visible := false;  //Show all checkout buttons
   confirm_false.Visible := false;
  end;
end;

end.

