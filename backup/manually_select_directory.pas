unit Manually_Select_Directory;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TManualDirectory }

  TManualDirectory = class(TForm)
    Dirc_Select: TSelectDirectoryDialog;
    Select_Dirc_BTN: TButton;
    Done_INPUT_PATH: TButton;
    Des: TLabel;
    Input_PATH: TEdit;
    procedure Done_INPUT_PATHClick(Sender: TObject);
    procedure Select_Dirc_BTNClick(Sender: TObject);
  private

  public

  end;

var
  ManualDirectory: TManualDirectory;

implementation
uses main_menu;
{$R *.lfm}

{ TManualDirectory }

procedure TManualDirectory.Select_Dirc_BTNClick(Sender: TObject);
begin
  if Dirc_Select.Execute then
     Input_PATH.Text := Dirc_Select.Filename;
end;

procedure TManualDirectory.Done_INPUT_PATHClick(Sender: TObject);
begin
     if fileexists((Input_PATH.Text + '\customer.txt')) then begin //Test via existance of filenames
            main_menu.POS.dirc_locate := Input_PATH.Text;
            main_menu.POS.Show;
            ManualDirectory.Hide;
     end
     else //When user inputted an incorrect directory
       showmessage('Files not found! Either the directory inputted is incorrect, or file has corrupted.');
end;
end.

