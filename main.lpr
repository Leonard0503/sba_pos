program main;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, tachartlazaruspkg, main_menu, Cart_Display, Manually_Select_Directory
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TManualDirectory, ManualDirectory);
  Application.CreateForm(TPOS, POS);
  Application.CreateForm(TCartForm, CartForm);
  Application.Run;
end.

