unit main_menu;
{---------------------------------------------------------}
{|                                                       |}
{|                                                       |}
{|                    Ho Lap College                     |}
{|          ICT SBA Part D : Software Developement       |}
{|            A Cashless Point-of-Sales System           |}
{|                 Name: Yeung Kai Chun                  |}
{|                   Class: F5D / F6D                    |}
{|                                                       |}
{|                                                       |}
{---------------------------------------------------------}
{$mode objfpc}{$H+}{$R+}{$Q+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, Grids, Menus, ValEdit, Spin, TAGraph, TASeries, Windows, TALegend, crt, Types
  ,dos;

type

  { TPOS }
  customers_base = record
    id:string[8];
    class_name,class_no:string[2];
    customer_name:string[20];
    gender,is_staff:char;
    balance:real;
    frequency_bought:integer;
    total_contribution:real;
    pass:boolean;
  end;
  items_base = record
    code:string[4];
    name:String[25];
    origin:string[25];
    import_price,selling_price:real;
    quantity, beginningpointer, endpointer:integer;
    purchase:boolean;
    frequency_bought:integer; //How many quantity this item has selled
    total_amount_contributed:real; //How much this item has contributed in terms of money
    males_contribution,females_contribution:real;
    pass:boolean
  end;
  transaction_properties = record
    ref_num:string[4];
    date,stdnum:string[8];
    cls:string;
    user_name: string;
    gender : char;
    transaction_amount : real;
    items_code : array[0..500] of string[4];
    corr_items_amount : array[0..500] of integer;
    items_bought : integer;
  end;
  comparing_type = record
    value : real;
    index : integer;
  end;
  output_type = record
    Name, value: array[0..9] of string;
  end;
  TPOS = class(TForm)
    admin_pass_in_des: TLabel;
    add_cart: TButton;
    admin_pass_input: TEdit;
    admin_des: TLabel;
    sub_order_but: TButton;
    restock_menu_des: TLabel;
    Login_sub: TButton;
    sub_search: TButton;
    default_acpass: TLabel;
    admin_acc_input: TEdit;
    acc_in_des: TLabel;
    id_target: TEdit;
    input_item_id_des: TLabel;
    request_quantity_im: TLabel;
    Shape4: TShape;
    qty_select: TSpinEdit;
    stats_screen: TStringGrid;
    Restock: TTabSheet;
    import_items_list: TStringGrid;
    top10items: TRadioButton;
    M_top10items: TRadioButton;
    F_top10items: TRadioButton;
    std_top10money: TRadioButton;
    std_top10freq: TRadioButton;
    Select_Stats: TRadioGroup;
    Shape3: TShape;
    Stats: TTabSheet;
    use_discount_btn: TCheckBox;
    Checkout_btn: TButton;
    Cart_Access: TButton;
    Grand_Total_Des: TLabel;
    Select_Description: TLabel;
    select_quantity: TLabel;
    Price_Show: TLabel;
    Logout_btn: TButton;
    cancel_highlight: TButton;
    acc_input: TEdit;
    Name_Welcome: TLabel;
    Rice_Group: TCheckGroup;
    Sandwich_Group: TCheckGroup;
    Drink_Group: TCheckGroup;
    SandF_Group: TCheckGroup;
    select_highlight: TLabel;
    POS_Tell: TLabel;
    Page_Selection: TPageControl;
    Drink: TRadioButton;
    SandF: TRadioButton;
    Rice: TRadioButton;
    Sandwich: TRadioButton;
    School_Name: TLabel;
    Main_Menu_Des: TTabSheet;
    Shape1: TShape;
    acc_input_des: TStaticText;
    Quantity_Helper: TSpinEdit;
    Shape2: TShape;
    Stock: TTabSheet;
    info_table: TStringGrid;
    Purchase: TTabSheet;
    Latest_Selection: TStringGrid;
    procedure acc_inputChange(Sender: TObject);
    procedure add_cartClick(Sender: TObject);
    procedure admin_pass_inputKeyPress(Sender: TObject; var Key: char);
    procedure cancel_highlightClick(Sender: TObject);
    procedure Cart_AccessClick(Sender: TObject);
    procedure Checkout_btnClick(Sender: TObject);
    procedure DrinkClick(Sender: TObject);
    procedure Drink_GroupItemClick(Sender: TObject; Index: integer);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure F_top10itemsClick(Sender: TObject);
    procedure Login_subClick(Sender: TObject);
    procedure Logout_btnClick(Sender: TObject);
    procedure M_top10itemsClick(Sender: TObject);
    procedure PurchaseHide(Sender: TObject);
    procedure PurchaseShow(Sender: TObject);
    procedure qty_selectChange(Sender: TObject);
    procedure Quantity_HelperChange(Sender: TObject);
    procedure RestockShow(Sender: TObject);
    procedure RiceClick(Sender: TObject);
    procedure Rice_GroupItemClick(Sender: TObject; Index: integer);
    procedure SandFClick(Sender: TObject);
    procedure SandF_GroupItemClick(Sender: TObject; Index: integer);
    procedure SandwichClick(Sender: TObject);
    procedure Sandwich_GroupItemClick(Sender: TObject; Index: integer);
    procedure StatsEnter(Sender: TObject);
    procedure std_top10freqClick(Sender: TObject);
    procedure std_top10moneyClick(Sender: TObject);
    procedure StockShow(Sender: TObject);
    procedure sub_order_butClick(Sender: TObject);
    procedure sub_searchClick(Sender: TObject);
    procedure top10itemsClick(Sender: TObject);
    procedure use_discount_btnChange(Sender: TObject);
  private

  public
     dirc_locate : string;
     Current_User_Info : customers_base;
     is_checkingout : boolean;
     db_items : array[0..1500] of items_base;
     db_customer : array[0..1500] of customers_base;
     db_transaction : array[0..1500] of transaction_properties;
     no_of_items : integer; //number of items in the database
     no_of_customers : integer; //number of students in the database
     processed_data : array[0..4] of output_type;
     {Index references:
      0 : Top 10 items
      1 : Top 10 items among males
      2 : Top 10 items among females
      3 : Top 10 students with purchasing amount
      4 : Top 10 students with purchasing frequency}
     procedure clearpurchasecache;
     procedure init_and_clear_purchase_page;
     procedure read_in_necessary_files;
  end;

var
  POS: TPOS;
implementation
uses Cart_Display,Manually_Select_Directory;
type
  pointers_of_each_type = record
    beginningpointer, endpointer : integer;
  end;

var
  no_of_rice,no_of_sandf,no_of_sandwich,no_of_drink : integer;
  drink_pointer, sandf_pointer, rice_pointer, sandwich_pointer : pointers_of_each_type;
  //Containing the index of items pointer to classify starting/ending pointer of every items' type
{$R *.lfm}

{ TPOS }

function deletingspace(tempstr:string):string;
{A function that remove the blank space at the end of tempstr}
var
  pointers: integer;
  difference : boolean = false;
begin
    pointers := length(tempstr); //Pointing at the end of tempstr
      while (not difference) and (pointers >= 1) do begin
        if tempstr[pointers] <> ' ' then begin
          difference := true;
          tempstr := copy(tempstr,1,pointers);
        end
        else
          dec(pointers);
      end;
    deletingspace := tempstr;
end;

function find_begin(findletter:String):integer;
//function that find the beginning of a specific items type
var
  found : boolean = false;
  r_value : integer = 0;
begin
  while not found do begin
    if findletter = pos.db_items[r_value].code[1] then //When found matched first letter
       found := true
    else
       inc(r_value);
  end;
  find_begin := r_value; //Returning functions' required data
end;

function find_end(findletter:String; beginpointer:integer):integer;
//function that find the end of a specific items type
var
  found : boolean = false;
  r_value : integer;
begin
  r_value := beginpointer;
  while not found do begin
    if findletter <> pos.db_items[r_value].code[1] then //When detected a change in first letter
       found := true
    else
       inc(r_value);
  end;
  find_end := r_value; //Returning functions' required data
end;

procedure displaydefault;
var
  infile : text;
  i, j: integer;
  read_location : string;
  found : boolean = false;
begin
    i := 0;
    if (not fileexists('2020\DB\items.txt')) then
       ManualDirectory.Show
    else
       POS.dirc_locate := '2020\DB\';
    system.assign(infile, POS.dirc_locate + '\items.txt');
    system.reset(infile);
    while not eof(infile) do begin
      with pos.db_items[i] do
           system.read(infile,code,name,origin,import_price,selling_price,quantity);
      system.readln(infile);
      inc(i);
    end;
    system.close(infile);  //Closing file to prevent memeory loss
    pos.no_of_items := i;
    for i := 0 to pos.no_of_items do
        pos.db_items[i].name := deletingspace(pos.db_items[i].name);
    pos.info_table.rowcount := pos.no_of_items + 1;
    for i := 1 to 4 do begin
      for j := 1 to pos.no_of_items do begin
        case i of  //Write data into cells according to the value i
          1 : pos.info_table.Cells[i,j] := pos.db_items[j - 1].code;
          2 : pos.info_table.Cells[i,j] := pos.db_items[j - 1].name;
          3 : pos.info_table.Cells[i,j] := '$'+ floattostr(pos.db_items[j - 1].selling_price);
          4 : pos.info_table.Cells[i,j] := inttostr(pos.db_items[j - 1].quantity);
        end; //End of case
      end;
    end; //End of cells value placement
end;

procedure TPOS.StockShow(Sender: TObject);//Read data from DB and display on different cells
begin
   displaydefault; //When form is shown, read DB and deplay all info
end;

procedure assigning_cells(beginningpointer,endpointer:integer);
//A procedure that scale a table and set the cells' values in the table
var
  j,k:integer;
begin
  pos.info_table.rowcount := endpointer - beginningpointer + 1;
  for j := 1 to 4 do begin
         for k := 1 to (endpointer - beginningpointer) do begin
             case j of  //Write data into cells according to the value i
                  1 : pos.info_table.Cells[j,k] := pos.db_items[beginningpointer + k - 1].code;
                  2 : pos.info_table.Cells[j,k] := pos.db_items[beginningpointer + k - 1].name;
                  3 : pos.info_table.Cells[j,k] := '$'+ floattostr(pos.db_items[beginningpointer + k - 1].selling_price);
                  4 : pos.info_table.Cells[j,k] := inttostr(pos.db_items[beginningpointer + k - 1].quantity);
             end; //End of case
         end;
     end;
end;

procedure TPOS.RiceClick(Sender: TObject);
begin
   assigning_cells(no_of_drink + no_of_sandf,no_of_drink + no_of_sandf + no_of_rice);
end;

procedure TPOS.SandFClick(Sender: TObject);
begin
   assigning_cells(no_of_drink ,no_of_drink + no_of_sandf);
end;

procedure TPOS.SandwichClick(Sender: TObject);
begin
   assigning_cells(no_of_drink + no_of_sandf + no_of_rice,no_of_items);
end;

procedure TPOS.DrinkClick(Sender: TObject);
begin
   assigning_cells(0, no_of_drink);
end;

procedure TPOS.FormActivate(Sender: TObject);
var
  beginningpointer, endpointer : integer;
begin
  Page_Selection.ActivePage := Stock;
  {Because reading customers' information has dependency on activiating items avaliable
  first as shwon in procedure TPOS.PurchaseShow, therefore stock must be opened first
  to aviod lacking of data}
  displaydefault;  //Re-execute this procedure to read in data
   beginningpointer := find_begin('R');
   endpointer := find_end('R',beginningpointer);
   no_of_rice := endpointer - beginningpointer;
   rice_pointer.beginningpointer := beginningpointer;
   rice_pointer.endpointer := endpointer;
   beginningpointer := find_begin('F');
   endpointer := find_end('F',beginningpointer);
   no_of_sandf := endpointer - beginningpointer;
   sandf_pointer.beginningpointer := beginningpointer;
   sandf_pointer.endpointer := endpointer;
   beginningpointer := find_begin('S');
   endpointer := find_end('S',beginningpointer);
   no_of_sandwich := endpointer - beginningpointer;
   sandwich_pointer.beginningpointer := beginningpointer;
   sandwich_pointer.endpointer := endpointer;
   beginningpointer := find_begin('D');
   endpointer := find_end('D',beginningpointer);
   no_of_drink := endpointer - beginningpointer;
   drink_pointer.beginningpointer := beginningpointer;
   drink_pointer.endpointer := endpointer;
   Page_Selection.ActivePage := Main_Menu_Des; //Showing Main Menu default
   no_of_items := 0;
   no_of_customers := 0; //initialize these two variables
end;

procedure TPOS.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  application.Terminate;
end;

procedure TPOS.cancel_highlightClick(Sender: TObject);
begin
   SandF.checked := false;
   Rice.checked := false;
   Sandwich.checked := false;
   Drink.checked := false;  //Unchecking all button activated
   displaydefault; //reseting view to default
end;

procedure TPOS.Cart_AccessClick(Sender: TObject);
begin
  is_checkingout := false;
  CartForm.Showmodal;
end;

procedure TPOS.Checkout_btnClick(Sender: TObject);
begin
  is_checkingout := true;
  CartForm.ShowModal;
end;

procedure TPOS.clearpurchasecache; //Procedure that clear all temp. login status
var
  i : integer;
begin
  with pos do begin
    Drink_Group.Enabled := false;
    SandF_Group.Enabled := false;
    Rice_Group.Enabled := false;
    Sandwich_Group.Enabled := false;
    Logout_btn.Visible := false;
    acc_input.text := '';
    Name_Welcome.Caption := '';
    price_show.Caption := '$0.0';
    for i := 0 to no_of_items do
        db_items[i].purchase := false;
    Quantity_Helper.Value := 1;
    Quantity_Helper.ReadOnly := true;
    Latest_Selection.Clean([gzNormal, gzFixedRows]); //Clean all cells except header
    CartForm.Main_List.Clean([gzNormal, gzFixedRows]); //Clean all cells except header
    CartForm.Main_List.RowCount := 1;
    CartForm.Total_Price.Caption := '$0.0';
    Price_Show.Caption := '$0.0';
    use_discount_btn.Enabled := false; //Reset discount button
    use_discount_btn.Checked := false;
    Cartform.confirm_pay.Caption := '';
    Cartform.confirm_true.Visible := false;
    Cartform.confirm_false.Visible := false;
    Checkout_btn.Visible := false;
    add_cart.Visible := true;
    Cartform.Height := 756;
    ZeroMemory(@current_user_info,SizeOf(current_user_info)); //Clear all current user info
  end;
end;

procedure TPOS.init_and_clear_purchase_page;
{Clear all checkbox upon exit to prevent duplicated items}
{Also clear some error text upon initial creation of purchase page}
begin
  with pos do begin
   Drink_Group.items.Clear;
   SandF_Group.items.Clear;
   Rice_Group.items.Clear;
   Sandwich_Group.items.Clear;
   Quantity_Helper.Value := 1;
   Quantity_Helper.ReadOnly := true;
   Checkout_btn.Visible := false;
   add_cart.Visible := true;
  end;
end;

procedure TPOS.read_in_necessary_files; //reading in files
var
   i, no_of_D,no_of_F,no_of_R: integer;
  DB : text;
begin
  no_of_D := 0;
  no_of_F := 0;
  no_of_R := 0;
  if POS.dirc_locate = '' then  //Test if files are correctly targeted
    if (not fileexists('J:\SBA\2020\DB\items.txt')) then
       ManualDirectory.Show
    else
       POS.dirc_locate := 'J:\SBA\2020\DB\';
  displaydefault;
  with pos do begin
    for i := 0 to no_of_items do
        case db_items[i].code[1] of
             'D': begin
             Drink_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then //Disable to check if none quantity reamaining
               Drink_Group.CheckEnabled[i] := false;
             inc(no_of_D);
             end;
             'F': begin
             sandf_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               sandf_Group.CheckEnabled[i - no_of_D] := false;
             inc(no_of_F);
             end;
             'R': begin
             Rice_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               Rice_Group.CheckEnabled[i - no_of_F - no_of_D] := false;
             inc(no_of_R);
             end;
             'S': begin
             Sandwich_Group.items.Add(db_items[i].name);
             if db_items[i].quantity <= 0 then
               Sandwich_Group.CheckEnabled[i - no_of_D - no_of_F - no_of_R] := false;
             end;
        end;
    Drink_Group.Enabled := false;
    SandF_Group.Enabled := false;
    Rice_Group.Enabled := false;
    Sandwich_Group.Enabled := false;
  end;
  //Reading all customers' infomation
  system.assign(DB, POS.dirc_locate + '\customer.txt');
  system.reset(DB);
  no_of_customers := 0;
  while not eof(DB) do begin
    with db_Customer[no_of_customers] do
        system.read(DB,id,class_name,class_no,customer_name,gender,is_staff,balance);
    system.readln(DB);
    inc(no_of_customers);
    end;
  system.close(DB);
  for i := 0 to no_of_items do
        db_items[i].purchase := false;
  for i := 0 to no_of_customers do
        db_Customer[i].customer_name := deletingspace(db_Customer[i].customer_name);
end;

procedure TPOS.PurchaseShow(Sender: TObject);
begin
   read_in_necessary_files;
end;

procedure TPOS.Quantity_HelperChange(Sender: TObject);
{If user requested an unreasonable quantity of a specific items, i.e. choose below 1
 or choose higher than the avaliable quantity, reset quantity requested}
begin
  if Quantity_Helper.Value <= 0 then begin
    Quantity_Helper.Value := 1;
    showmessage('Requested quantity CANNOT be lower then 1! ');
  end
  else if Quantity_Helper.Value > strtoint(Latest_Selection.Cells[3,1]) then begin
    Quantity_Helper.Value := 1;
    showmessage('Requested quantity exceed avaliable quantity! ');
  end;
end;

procedure recalculate_grand_price;
var
  i : integer;
 temp_price : real = 0;
begin
  for i := 1 to CartForm.Main_List.RowCount - 1 do
     temp_price := temp_price + strtofloat(CartForm.Main_List.Cells[4,i]);
  if pos.use_discount_btn.Checked then
    temp_price := temp_price * 0.9; //Give 10% off discount
  Cartform.Total_Price.Caption := '$' + floattostrf(temp_price,ffFixed,1,1);;
  pos.Price_Show.Caption := '$' + floattostrf(temp_price,ffFixed,1,1); //Only show 1 d.p.
end;

procedure changecart(items_pointer:integer);
var
 i : integer = 1;
 found : boolean = false;
begin
  with CartForm.Main_List do begin
   if pos.Latest_Selection.Cells[1,1] <> '' then begin
     while (not found) and (i <= RowCount  - 1) and (RowCount >= 2) do begin //Find if the item awaiting to be added is dupilcated in Cart
       if pos.Latest_Selection.Cells[2,1] = Cells[2,i] then
          found := true
       else
          inc(i);
     end;
   if found then begin//Add requsted quantity to the current quantity, then display it
       Cells[3,i] := inttostr(strtoint(Cells[3,i]) + pos.Quantity_Helper.Value);
       if strtoint(Cells[3,i]) > pos.db_items[items_pointer].quantity then begin
          Cells[3,i] := inttostr(pos.db_items[items_pointer].quantity);
          showmessage('Requested quantity exceed avaliable quantity! Resetting requested quantity to maximum avalible quantity');
       end;
       Cells[4,i] := floattostr(pos.db_items[items_pointer].selling_price * strtoint(Cells[3,i]));
       //Recalculate SubTotal
       recalculate_grand_price;
   end
   else begin
       RowCount := RowCount + 1;
       Cells[1,RowCount - 1] := pos.Latest_Selection.Cells[1,1];
       Cells[2,RowCount - 1] := pos.Latest_Selection.Cells[2,1];
       Cells[3,RowCount - 1] := inttostr(pos.Quantity_Helper.Value);
       Cells[4,RowCount - 1] := floattostr((pos.db_items[items_pointer].selling_price) *
                             pos.Quantity_Helper.Value);
       recalculate_grand_price;
       pos.Latest_Selection.Cells[4,1] := 'In Cart';
       pos.Latest_Selection.Columns.Items[3].Font.Color:= clLime;
       pos.db_items[items_pointer].purchase := true;
     end;
   end
   else
     showmessage('No Items were selected.');
  end;
end;

function Capitalize_first_letter_of_id:string;
var
  tempchar:char;
begin
  tempchar := pos.acc_input.text[1];
  if tempchar = 's' then
     tempchar := chr(ord(tempchar) - 32);
  Capitalize_first_letter_of_id := tempchar + copy(pos.acc_input.text,2,8);
end;

procedure TPOS.use_discount_btnChange(Sender: TObject);
begin
  if use_discount_btn.Checked then //Notifying that the user is currently using staff discount
    cart_display.CartForm.use_discount_notice.Caption := 'Using staff discount, 10% off'
  else
    cart_display.CartForm.use_discount_notice.Caption := '';
  recalculate_grand_price; //Showing price after discount / cancelling discount
  is_checkingout := true;
end;

procedure TPOS.PurchaseHide(Sender: TObject);
begin
  init_and_clear_purchase_page; //Clear pages upon changing of tabs
  clearpurchasecache; //Disabling all buttons and wait for Login_sub
end;

procedure TPOS.acc_inputChange(Sender: TObject);
var
  found : boolean = false;
  i : integer = 0;
  temp : string[8]; //To store a temperaily ID with the first S being capitalized
begin
  if length(acc_input.text) = 8 then begin
     temp := Capitalize_first_letter_of_id; //Capitalize the 's' in the first letter of customer.id
     while (not found) and (i <= no_of_customers) do begin
       if db_customer[i].id = temp then begin
         found := true;
         Current_User_Info := db_customer[i]; //Passing the customer's info to checkout form
         Name_Welcome.Caption := 'Welcome, ' + db_customer[i].customer_name + '!';
         Drink_Group.Enabled := true;
         SandF_Group.Enabled := true;
         Rice_Group.Enabled := true;
         Sandwich_Group.Enabled := true;
         Logout_btn.Visible := true;
         Checkout_btn.Visible := true;
         add_cart.Visible := true;
         if db_customer[i].is_staff = 'Y' then //Allow user to use discount if they are staffs
           use_discount_btn.Enabled := true;
       end
       else
         inc(i);
     end;
  end;
  if (length(acc_input.text) = 8) and (not found) then
     {If the user has enter an 8-digit ID but does not match with the database,
     return this sentence}
     Name_Welcome.Caption := 'ID not found. Please confirm your id!';
end;

procedure TPOS.add_cartClick(Sender: TObject);
var
  i : integer = 0;
  found : boolean = false;
begin
  while (not found) and (I <= no_of_items) and (Latest_Selection.Cells[4,1] <> 'Removed') do
      {If the user de-selected an item and try to add to cart again, prevent it
      from adding it into the cart as checkbox will be messed up}
      if db_items[i].code = Latest_Selection.Cells[1,1] then begin
        changecart(i);
        found := true;
      end
      else
      inc(i);
end;

procedure TPOS.Logout_btnClick(Sender: TObject);
begin
  clearpurchasecache; //Clear all info when the user click logout
end;

procedure SelectedItemsChange(increaseprice:boolean;i:integer);
var
  j : integer = 1;
  k : integer = 0;
  found : boolean = false;
begin
  with pos do begin
    Quantity_Helper.Value := 1;
    Quantity_Helper.ReadOnly := false;
    Quantity_Helper.MaxValue := db_items[i].quantity;
    if Latest_Selection.Cells[4,1] = 'Pending' then begin //If user didn't put the previous item into cart, uncheck that items
       while (not found) and (k <= Drink_Group.Items.Count - 1) do  //Search in Drink group
           if Latest_Selection.Cells[2,1] = Drink_Group.Items[k] then begin
             found := true;
             Drink_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= SandF_Group.Items.Count - 1) do //Search in SandF Group, skip if found in Drink group
           if Latest_Selection.Cells[2,1] = SandF_Group.Items[k] then begin
             found := true;
             SandF_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= Rice_Group.Items.Count - 1) do  //Search in Rice Group, skip if found in Drink or SandF group
           if Latest_Selection.Cells[2,1] = Rice_Group.Items[k] then begin
             found := true;
             Rice_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
       while (not found) and (k <= Sandwich_Group.Items.Count - 1) do //Search in Sandwich Group, skip if found in Drink, SandF or Rice group
           if Latest_Selection.Cells[2,1] = Sandwich_Group.Items[k] then begin
             found := true;
             Sandwich_Group.Checked[k] := false;
           end
           else
             inc(k);
       k := 0;
    end;
    found := false; //Reset found to False to prevent error if removing a cart items
    if increaseprice then begin //If the users selected another items
       Latest_Selection.Cells[1,1] := db_items[i].code;  //Adding information to latest selection
       Latest_Selection.Cells[2,1] := db_items[i].name;
       Latest_Selection.Cells[3,1] := inttostr(db_items[i].quantity);
       Latest_Selection.Columns.Items[3].Font.Color:= $0011A2DF;
       Latest_Selection.Cells[4,1] := 'Pending';
    end
    else begin //If the users de-selected an items
       Quantity_Helper.Value := 1;
       Latest_Selection.Columns.Items[3].Font.Color:= clRed;
       Latest_Selection.Cells[4,1] := 'Removed';
       if CartForm.Main_List.RowCount <> 2 then begin //Only execute these code if there's more than 1 item in the cart
         while (not found) and (j <= CartForm.Main_List.RowCount - 1) do begin
             //Removing items from cart and shifting others items blow to the top
           if CartForm.Main_List.Cells[2,j] = db_items[i].name then begin
             db_items[i].purchase := false;
             if j = CartForm.Main_List.RowCount - 1 then //If the items that need to be removed is on the last row
               CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1
             else begin
              for k := j to CartForm.Main_List.RowCount - 1 do begin
                  with CartForm.Main_List do begin
                     Cells[1,k-1] := Cells[1,k]; //shifting the next cells' value to the current cells
                     Cells[2,k-1] := Cells[2,k];
                     Cells[3,k-1] := Cells[3,k];
                     Cells[4,k-1] := Cells[4,k];
                  end;
              end;
              found := true;
              CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1; //Delete Last Row
             end;
           end
           else
               inc(j);
         end;
      end
       else if CartForm.Main_List.RowCount = 2 then begin
         CartForm.Main_List.RowCount := CartForm.Main_List.RowCount - 1; //If there's only 1 item in the cart, just remove a row
         Cartform.Total_Price.Caption := '$0.0'; //Reseting Grand Total to $0.0
       end;
      recalculate_grand_price;
    end;
  end;
end;

procedure TPOS.Drink_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Drink_Group.Items.Count - 1 do begin
     if (Drink_Group.Checked[i]) and (not db_items[i + drink_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i)
    else if (not Drink_Group.Checked[i]) and (db_items[i + drink_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i);
  end;
end;

procedure TPOS.SandF_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to SandF_Group.Items.Count - 1 do begin
     if (SandF_Group.Checked[i]) and (not db_items[i + SandF_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + SandF_pointer.beginningpointer)
    else if (not SandF_Group.Checked[i]) and (db_items[i + SandF_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + SandF_pointer.beginningpointer);
  end;
end;

procedure TPOS.Rice_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Rice_Group.Items.Count - 1 do begin
     if (Rice_Group.Checked[i]) and (not db_items[i + Rice_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + Rice_pointer.beginningpointer)
    else if (not Rice_Group.Checked[i]) and (db_items[i + Rice_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + Rice_pointer.beginningpointer);
  end;
end;
procedure TPOS.Sandwich_GroupItemClick(Sender: TObject; Index: integer);
var
  i : integer = 0;
begin
  for i := 0 to Sandwich_Group.Items.Count - 1 do begin
     if (Sandwich_Group.Checked[i]) and (not db_items[i + Sandwich_pointer.beginningpointer].purchase) then
        SelectedItemsChange(true,i + Sandwich_pointer.beginningpointer)
    else if (not Sandwich_Group.Checked[i]) and (db_items[i + Sandwich_pointer.beginningpointer].purchase) then
        SelectedItemsChange(false,i + Sandwich_pointer.beginningpointer);
  end;
end;

procedure reset_item_pass;
var i : integer;
begin
  for i := 0 to pos.no_of_items do
    pos.db_items[i].pass := false;
end;
procedure reset_customers_pass;
var i : integer;
begin
  for i := 0 to pos.no_of_customers do
    pos.db_customer[i].pass := false;
end;

procedure processing_stats(no_of_transaction: integer);
var
  f,i,j,l: integer;
  temp_max : comparing_type;
  found : boolean = false;
  pass_j:boolean = false;
begin
  with pos do begin
  for i := 0 to no_of_items do begin //initalize db_items[j].frequency_bought and db_items[j].total_amount_contributed for later analyze
    db_items[i].frequency_bought := 0;
    db_items[i].total_amount_contributed := 0;
  end;
  for i := 0 to no_of_transaction do begin //initalize
    db_items[i].males_contribution := 0;
    db_items[i].females_contribution := 0;
  end;
  for i := 0 to no_of_customers do begin
    db_customer[i].frequency_bought := 0;
    db_customer[i].total_contribution := 0;
  end;
    for i := 0 to no_of_transaction do begin //transaction #
      for f := 0 to db_transaction[i].items_bought - 1 do begin //no. of items in this transactions
      l := 0;
      j := 0;
      found := false;
      while (l <= no_of_customers) and (not found) do begin//finding the person who is involved in this transaction
        if db_transaction[i].stdnum = db_customer[l].id then begin
          inc(db_customer[l].frequency_bought); //add one transaction record to this person
          db_customer[l].total_contribution := db_customer[l].total_contribution
          + (db_items[j].selling_price * db_transaction[i].corr_items_amount[f]);
          found := true
        end
        else
          inc(l);
      end;
        while (J <= no_of_items) and (not pass_j) do begin //corrsponding items in database
          if db_transaction[i].items_code[f] = db_items[j].code then begin
            db_items[j].frequency_bought := db_items[j].frequency_bought + db_transaction[i].corr_items_amount[f];
            //Add all the amount of this items bought for further calculation
            db_items[j].total_amount_contributed := db_items[j].selling_price * db_items[j].frequency_bought;
            if db_transaction[i].gender = 'M' then
              db_items[j].males_contribution := db_items[j].males_contribution +
              (db_items[j].selling_price * db_transaction[i].corr_items_amount[f])
            else if db_transaction[i].gender = 'F' then
              db_items[j].females_contribution := db_items[j].females_contribution +
              (db_items[j].selling_price * db_transaction[i].corr_items_amount[f]);
            pass_j := true;
          end
          else
            inc(j);
        end; //end of j
      pass_j := false; //reset pass_j
      j := 0;
      end; //end of f
    end; //end of i
    for i := 0 to 4 do begin
      case i of
           0 : begin //top 10 items
             reset_item_pass; //initialize
             for j := 0 to 9 do begin
               temp_max.Index := 0;
               while db_items[temp_max.Index].pass do
                   inc(temp_max.Index); //move to another index if 0 is one of the top 10 items
               for f := 0 to no_of_items do begin
                if (db_items[temp_max.index].total_amount_contributed < db_items[f].total_amount_contributed)
                and (not db_items[f].pass) then
                  temp_max.index := f;
               end;
               processed_data[0].Name[j] := db_items[temp_max.index].name;
               processed_data[0].value[j] := '$' + floattostr(db_items[temp_max.index].total_amount_contributed);
               db_items[temp_max.index].pass := true;
             end;
             reset_item_pass;
           end;
           1 : begin //top 10 among males
            for j := 0 to 9 do begin
               temp_max.Index := 0;
               while db_items[temp_max.Index].pass do
                   inc(temp_max.Index);
               for f := 0 to no_of_items do begin
                if (db_items[temp_max.index].males_contribution < db_items[f].males_contribution)
                and (not db_items[f].pass) then
                  temp_max.index := f;
               end;
               processed_data[1].Name[j] := db_items[temp_max.index].name;
               processed_data[1].value[j] := '$' + floattostr(db_items[temp_max.index].males_contribution);
               db_items[temp_max.index].pass := true;
             end;
            reset_item_pass;
            end;
           2 : begin //top 10 among females
            for j := 0 to 9 do begin
               temp_max.Index := 0;
               while db_items[temp_max.Index].pass do
                   inc(temp_max.Index);
               for f := 0 to no_of_items do begin
                if (db_items[temp_max.index].females_contribution < db_items[f].females_contribution)
                and (not db_items[f].pass) then
                  temp_max.index := f;
               end;
               processed_data[2].Name[j] := db_items[temp_max.index].name;
               processed_data[2].value[j] := '$' + floattostr(db_items[temp_max.index].females_contribution);
               db_items[temp_max.index].pass := true;
             end;
             reset_item_pass;
          end;
          3 : begin //Top 10 students with purchasing amount
            reset_customers_pass;
            for j := 0 to 9 do begin
               temp_max.Index := 0;
               while db_customer[temp_max.Index].pass do
                   inc(temp_max.Index); //move to another index if 0 is one of the top 10 items
               for f := 0 to no_of_items do begin
                if (db_customer[temp_max.index].frequency_bought < db_customer[f].frequency_bought)
                and (not db_customer[f].pass) then
                  temp_max.index := f;
               end;
               processed_data[3].Name[j] := db_customer[temp_max.index].customer_name;
               processed_data[3].value[j] := floattostr(db_customer[temp_max.index].frequency_bought) + ' Transactions';
               db_customer[temp_max.index].pass := true;
             end;
             reset_customers_pass;
            end;
          4 : begin //Top 10 students with purchasing freqency
            for j := 0 to 9 do begin
               temp_max.Index := 0;
               while db_customer[temp_max.Index].pass do
                   inc(temp_max.Index); //move to another index if 0 is one of the top 10 items
               for f := 0 to no_of_items do begin
                if (db_customer[temp_max.index].total_contribution < db_customer[f].total_contribution)
                and (not db_customer[f].pass) then
                  temp_max.index := f;
               end;
               processed_data[4].Name[j] := db_customer[temp_max.index].customer_name;
               processed_data[4].value[j] := '$' + floattostr(db_customer[temp_max.index].total_contribution);
               db_customer[temp_max.index].pass := true;
             end;
             reset_customers_pass;
          end;
      end; //end of case
    end; //end of for
  end; //end of with
end;

procedure TPOS.top10itemsClick(Sender: TObject);
var i : integer;
begin
  for i := 1 to 10 do begin
    stats_screen.Cells[1,i] := processed_data[0].Name[i-1];
    stats_screen.Cells[2,i] := processed_data[0].value[i-1];
  end;
end;
procedure TPOS.M_top10itemsClick(Sender: TObject);
var i : integer;
begin
  for i := 1 to 10 do begin
    stats_screen.Cells[1,i] := processed_data[1].Name[i-1];
    stats_screen.Cells[2,i] := processed_data[1].value[i-1];
  end;
end;

procedure TPOS.F_top10itemsClick(Sender: TObject);
var i : integer;
begin
  for i := 1 to 10 do begin
    stats_screen.Cells[1,i] := processed_data[2].Name[i-1];
    stats_screen.Cells[2,i] := processed_data[2].value[i-1];
  end;
end;

procedure TPOS.std_top10freqClick(Sender: TObject);
var i : integer;
begin
  for i := 1 to 10 do begin
    stats_screen.Cells[1,i] := processed_data[3].Name[i-1];
    stats_screen.Cells[2,i] := processed_data[3].value[i-1];
  end;
end;
procedure TPOS.std_top10moneyClick(Sender: TObject);
var i : integer;
begin
  for i := 1 to 10 do begin
    stats_screen.Cells[1,i] := processed_data[4].Name[i-1];
    stats_screen.Cells[2,i] := processed_data[4].value[i-1];
  end;
end;
procedure TPOS.StatsEnter(Sender: TObject);
var
  infile : text;
  temp : char; //storing space
  i : integer = 0;
  j : integer = 0;
  k : integer;
begin
  system.assign(infile,POS.dirc_locate + '\transact.txt');
  system.reset(infile);
  while not eof(infile) do begin
    with db_transaction[i] do begin
      read(infile,ref_num);
      read(infile,temp); //avoid space
      read(infile,date);
      read(infile,temp); //avoid space
      read(infile,stdnum);
      read(infile,transaction_amount);
      read(infile,temp); //avoid space
      j := 0;
      while not eoln(infile) do begin
        read(infile,items_code[j]);
        read(infile,temp); //avoid space
        read(infile,corr_items_amount[j]);
        read(infile,temp); //avoid space
        inc(j);
      end;
      items_bought := j;
      for k := 0 to no_of_customers do begin //compare the current customer id with the databse
        if stdnum = db_customer[k].id then begin
          cls := db_customer[k].class_name;
          gender := db_customer[k].gender;
          user_name := db_customer[k].customer_name;
          break; //stop this for loop once a matched data is found
        end;
      end;
      inc(i);
      readln(infile);
    end;
  end;
  system.close(infile);
  processing_stats(i);
end;
procedure restock_reset;
begin
  with pos do begin
    admin_des.Visible := true;
    default_acpass.Visible := true;
    admin_acc_input.Visible := true;
    acc_in_des.Visible := true;
    Shape4.Visible := true;
    admin_pass_input.Visible := true;
    admin_pass_in_des.Visible := true;
    login_sub.Visible := true;
    input_item_id_des.Visible := false;
    id_target.Visible := false;
    request_quantity_im.Visible := false;
    qty_select.Visible := false;
    sub_search.Visible := false;
    import_items_list.Visible := false;
    restock_menu_des.Visible := false;
    sub_order_but.Visible := false;
    import_items_list.Clean([gzNormal, gzFixedRows]); //Clear the table
    import_items_list.RowCount := 1;
    admin_acc_input.Caption := '';
    admin_pass_input.Caption := '';
    id_target.Caption := '';
    qty_select.Value := 1;
  end;
end;

procedure restock_showmenu;
begin
  with pos do begin
    admin_des.Visible := not admin_des.Visible;
    default_acpass.Visible := not default_acpass.Visible;
    admin_acc_input.Visible := not admin_acc_input.Visible;
    acc_in_des.Visible := not acc_in_des.Visible;
    Shape4.Visible := not Shape4.Visible;
    admin_pass_input.Visible := not admin_pass_input.Visible;
    admin_pass_in_des.Visible := not admin_pass_in_des.Visible;
    login_sub.Visible := not login_sub.Visible;
    input_item_id_des.Visible := true;
    id_target.Visible := true;
    request_quantity_im.Visible := true;
    qty_select.Visible := true;
    sub_search.Visible := true;
    import_items_list.Visible := true;
    restock_menu_des.Visible := true;
    sub_order_but.Visible := true;
  end;
end;
procedure account_vari;
begin
  with pos do begin
    if (admin_pass_input.Caption = 'admin') and (admin_acc_input.Caption = 'admin') then
      restock_showmenu
    else
      showmessage('Incorrect account or password!');
  end;
end;

procedure TPOS.RestockShow(Sender: TObject);
begin
   restock_reset;
end;

procedure TPOS.Login_subClick(Sender: TObject);
begin
  account_vari;
end;

procedure TPOS.admin_pass_inputKeyPress(Sender: TObject; var Key: char);
//Login when user press enter also so the user does not have to press the login button
begin
   if ord(key) = 13 then
     account_vari
end;

procedure TPOS.qty_selectChange(Sender: TObject); //check if data entered was incorrect for quantity
begin
  try
     if qty_select.Value > 99 then begin
       showmessage('Selected quantity is over the limit! Try it again.');
       qty_select.Value := 1;
     end
     else if qty_select.Value < 1 then begin
       showmessage('Selected quantity must be greater than 0.');
       qty_select.Value := 1;
     end;
  except
    showmessage('Selected quantity must be integer');
    qty_select.Value := 1;
  end;
end;

procedure add_import_into_list(items_id,items_name,items_origin:string;import_price:real);
var
  i : integer = 1;
  found : boolean = false;
begin
  with pos.import_items_list do begin
    while (i <= RowCount - 1) and (not found) and (RowCount <> 1 {If none data in the stringgrid, skip the comparison}) do begin
      // Increase the requested quantity instead of creating a new data if it is already exist
      if Cells[1, i] = items_id then begin
        Cells[4, i] := inttostr(strtoint(Cells[4, i]) + pos.qty_select.Value);
        if strtoint(Cells[4, i]) > 99 then begin //Prevent exceeding limit
          Cells[4, i] := '99';
          showmessage('Item''s quantity already exceed limit.');
        end;
        Cells[5, i] := '$' + floattostr(strtoint(Cells[4, i]) * import_price);
        found := true;
      end
      else
        inc(i);
    end;
    if not found then begin
      RowCount := RowCount + 1;
      Cells[1, RowCount - 1] := items_id;
      Cells[2, RowCount - 1] := items_name;
      Cells[3, RowCount - 1] := items_origin;
      Cells[4, RowCount - 1] := inttostr(pos.qty_select.Value);
      Cells[5, RowCount - 1] := '$' + floattostr(pos.qty_select.Value * import_price);
    end;
  end;
end;

procedure TPOS.sub_searchClick(Sender: TObject);
var
  found : boolean = false;
  i : integer = 0;
begin
  while (not found) and (i <= no_of_items) and (id_target.Caption <> '') do begin
     if db_items[i].code = id_target.Caption then begin
        found := true;
        add_import_into_list(db_items[i].code,db_items[i].name,db_items[i].origin,db_items[i].import_price);
     end
     else
        inc(i);
  end;
  if not found then begin
     showmessage('Items not found. Please try it again!');
     id_target.Caption := '0';
  end;
end;

procedure TPOS.sub_order_butClick(Sender: TObject); //Run all procedures linked with restocking
var
  i,j : integer;
  found : boolean = false;
  outfile,infile : text;
  restock_ref_num,dummy: string;
  total_fee:real = 0;
  ref_no_zero:integer;
  yy,mm,md,wd:word;
begin
  with import_items_list do begin
    for i := 1 to RowCount - 1 do begin
      j := 0;
      while (j <= no_of_items) and (not found) do begin
        if db_items[j].code = Cells[1,i] then begin
          found := true;
          db_items[j].quantity := db_items[j].quantity + strtoint(Cells[4,i]);
          //Increase quantity so the system can show it to users
        end
        else
          inc(j);
      end;
    end;
  end;
  system.assign(outfile,POS.dirc_locate + '\items.txt');
  system.rewrite(outfile);
  for i := 0 to no_of_items - 1 do begin//Write back current items' data to the database
    with db_items[i] do
      system.writeln(outfile,code,name,' ':25 - length(name),origin,' ':26 - length(origin)
      ,import_price:5:1,selling_price:5:1,' ',quantity);
  end;
  system.close(outfile);
  system.assign(infile,POS.dirc_locate + '\purchase.txt');
  system.reset(infile);
  while not eof(infile) do begin
    system.read(infile,restock_ref_num,dummy);
    system.readln(infile);
  end;
  system.close(infile);
  ref_no_zero := strtoint(copy(restock_ref_num,1,4));
  restock_ref_num := inttostr(ref_no_zero + 1);
  for i := 1 to 4 - length(restock_ref_num) do
    restock_ref_num := '0' + restock_ref_num;
  getdate(yy,mm,md,wd);
  for i := 1 to import_items_list.RowCount - 1 do
    total_fee := total_fee + strtofloat(copy(import_items_list.Cells[5,i],2,length(import_items_list.Cells[5,i])));
    //ignore dollars signs
  system.Append(infile);
  system.write(infile,restock_ref_num, ' ',yy,mm,md,total_fee:7:1,' ');
  for i := 1 to import_items_list.RowCount - 1 do begin
    system.write(infile,import_items_list.Cells[1,i]);
    system.write(infile,import_items_list.Cells[4,i]:4);
  end;
  system.writeln(infile);
  system.close(infile);
  showmessage('Order has been placed.');
  restock_reset;
end;

end.
